// Dear emacs, this is -*- c++ -*-
// $Id: TauJetAccessors_v2.h 631748 2014-11-28 09:58:13Z janus $
#ifndef XAODDITAU_DITAUACCESSORS_V1_H
#define XAODDITAU_DITAUACCESSORS_V1_H

// Local include(s):
#include "xAODTau/versions/DiTauJet_v1.h"
#include "xAODTau/DiTauDefs.h"


namespace xAOD {

    namespace xAODDiTau {
      /// Helper function for managing accessors to id input and output variables defined as enums in TauDefs.h
      ///
      /// This function holds on to Accessor objects that can be used by each
      /// TauJet_v2 object at runtime to get/set id variable values on themselves.
      ///
      /// @param idvar The id variable for which an Accessor should be returned
      /// @returns A pointer to an Accessor if successful, <code>0</code> if not
      ///

      template <class T> SG::AuxElement::Accessor< T >* detailsAccessorV1( xAOD::DiTauJetParameters::Detail detail );

    #include "DiTauJetAccessors_v1.icc"
    }
} // namespace xAOD

#endif // XAODDITAU_TAUJETACCESSORS_V1_H