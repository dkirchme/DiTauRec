// Dear emacs, this is -*- c++ -*-
// $Id: TauDefs.h 638520 2015-01-09 13:21:05Z janus $
#ifndef XAODDITAU_DITAUDEFS_H
#define XAODDITAU_DITAUDEFS_H

// Local include(s):
#include "Math/Vector4D.h"
#include <bitset>

/// Namespace holding all the xAOD EDM classes
namespace xAOD {
  typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<float> > PtEtaPhiMVectorF;

    /* define type traits for xAOD Tau.
     * needs to be outside of the TauJet namespace
     */
    // empty return type for all types T.
    // Leads to a compilation failure if one tries to use it with other types then a specialization is given for.
    template <class T>
    struct xAODDiTAU_return_type;

    // specialization for ints
    template <>
    struct xAODDiTAU_return_type<int> { typedef int type; };

    // specialization for floats
    template <>
    struct xAODDiTAU_return_type<float> { typedef float type; };


namespace DiTauJetParameters
{
    //-------------------------------------------------------------------------
    // DO NOT CHANGE THE ORDER OF THE ENUMS!
    // You can add, but changing the order may lead to disaster!
    //-------------------------------------------------------------------------



    //-------------------------------------------------------------------------
    // Enum for ditau parameters 
    //-------------------------------------------------------------------------
    enum Detail
    {
        TauJetVtxFraction,

        R_jet,
        R_subjet,
        R_core

    };



}//end namespace DiTauJetParameters

}

#endif // XAODDITAU_TAUDEFS_H
