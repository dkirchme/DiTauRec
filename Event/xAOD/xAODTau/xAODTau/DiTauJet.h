// Dear emacs, this is -*- c++ -*-
// $Id: TauJet.h 631748 2014-11-28 09:58:13Z janus $
#ifndef XAODDITAU_DITAUJET_H
#define XAODDITAU_DITAUJET_H

// Local include(s):
#include "xAODTau/versions/DiTauJet_v1.h"

/// Namespace holding all the xAOD EDM classes
namespace xAOD {
  /// Definition of the current "Ditau version"
  typedef DiTauJet_v1 DiTauJet;

}

#endif // XAODDITAU_DITAUJET_H