// Dear emacs, this is -*- c++ -*-
// $Id: TauJetAuxContainer.h 631748 2014-11-28 09:58:13Z janus $
#ifndef XAODDiTAU_DiTAUJETAUXCONTAINER_H
#define XAODDiTAU_DiTAUJETAUXCONTAINER_H

// Local include(s):
#include "xAODTau/versions/DiTauJetAuxContainer_v1.h"

namespace xAOD {
   /// Definition of the current taujet auxiliary container
   ///
   /// All reconstruction code should attach the typedefed auxiliary
   /// container to the xAOD::TauJetContainer, so it will be easy to change
   /// the container type as we get new I/O technologies for these
   /// objects.
   ///
   typedef DiTauJetAuxContainer_v1 DiTauJetAuxContainer;
}

// Set up a CLID and StoreGate inheritance for the class:
#ifndef XAOD_STANDALONE
#include "SGTools/CLASS_DEF.h"
CLASS_DEF( xAOD::DiTauJetAuxContainer , 1305731819 , 1 )
#endif // not XAOD_STANDALONE

#endif // XAODDITAU_DITAUJETAUXCONTAINER_H