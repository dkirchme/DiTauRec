// Dear emacs, this is -*- c++ -*-
// $Id: TauJetContainer.h 631748 2014-11-28 09:58:13Z janus $
#ifndef XAODDITAU_DITAUJETCONTAINER_H
#define XAODDITAU_DITAUJETCONTAINER_H

// Local include(s):
#include "xAODTau/DiTauJet.h"
#include "xAODTau/versions/DiTauJetContainer_v1.h"

namespace xAOD {
   /// Definition of the current "Ditaujet container version"
   typedef DiTauJetContainer_v1 DiTauJetContainer;
}

// Set up a CLID for the container:
#ifndef XAOD_STANDALONE
#include "SGTools/CLASS_DEF.h"
CLASS_DEF( xAOD::DiTauJetContainer, 1281324766, 1 )
#endif // XAOD_STANDALONE

#endif // XAODDITAU_DITAUJETCONTAINER_H