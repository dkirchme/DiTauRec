// Dear emacs, this is -*- c++ -*-
// $Id: TauJetAuxContainer_v2.h 633643 2014-12-04 11:31:39Z janus $
#ifndef XAODDITAU_VERSIONS_DITAUJETAUXCONTAINER_V1_H
#define XAODDITAU_VERSIONS_DITAUJETAUXCONTAINER_V1_H

// System include(s):
#include <vector>
extern "C" {
#   include <stdint.h>
}


//local includes
// #include "xAODDiTau/TauDefs.h"

// EDM include(s):
#include "xAODCore/AuxContainerBase.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODPFlow/PFOContainer.h"

namespace xAOD {
 
 
    /// Temporary container used until we have I/O for AuxStoreInternal
    ///
    /// This class is meant to serve as a temporary way to provide an auxiliary
    /// store with Athena I/O capabilities for the tau EDM. Will be exchanged for
    /// a generic auxiliary container type (AuxStoreInternal) later on.
    ///
    /// @author Michel Janus <janus@cern.ch>
    ///
    ///
    class DiTauJetAuxContainer_v1 : public AuxContainerBase {

    public:
     /// Default constructor
     DiTauJetAuxContainer_v1();
 
    private:
     std::vector< float > pt;
     std::vector< float > eta;
     std::vector< float > phi;
     std::vector< float > m;


    typedef ElementLink< xAOD::JetContainer > JetLink_t;
    std::vector< JetLink_t > jetLink;

    typedef ElementLink< xAOD::VertexContainer > VertexLink_t;
    std::vector< VertexLink_t > vertexLink;
    std::vector< VertexLink_t > secondaryVertexLink;

    std::vector< float > R_jet;
    std::vector< float > R_subjet;
    std::vector< float > R_core;

    std::vector< std::vector< float > > subjet_pt;
    std::vector< std::vector< float > > subjet_eta;
    std::vector< std::vector< float > > subjet_phi;
    std::vector< std::vector< float > > subjet_e;

    std::vector< std::vector< float > > subjet_f_core;

    std::vector< float > TauJetVtxFraction;

    typedef std::vector< ElementLink< TrackParticleContainer > > TrackLink_t;
    std::vector< TrackLink_t > trackLinks;
    std::vector< TrackLink_t > isoTrackLinks;
    std::vector< TrackLink_t > otherTrackLinks;
 
       
   }; // class TauJetAuxContainer_v2
 
} // namespace xAOD

// Set up a CLID and StoreGate inheritance for the class:
#ifndef XAOD_STANDALONE
#include "SGTools/BaseInfo.h"
SG_BASE( xAOD::DiTauJetAuxContainer_v1, xAOD::AuxContainerBase );
#endif // not XAOD_STANDALONE

#endif // XAODDITAU_VERSIONS_DITAUJETAUXCONTAINER_V1_H