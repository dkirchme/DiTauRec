// Dear emacs, this is -*- c++ -*-
// $Id: TauJetContainer_v2.h 631748 2014-11-28 09:58:13Z janus $
#ifndef XAODDITAU_VERSIONS_DITAUCONTAINER_V1_H
#define XAODDITAU_VERSIONS_DITAUCONTAINER_V1_H

// Core include(s):
#include "AthContainers/DataVector.h"

// Local include(s):
#include "xAODTau/versions/DiTauJet_v1.h"

namespace xAOD {
   /// The container is a simple typedef for now
   typedef DataVector< xAOD::DiTauJet_v1 > DiTauJetContainer_v1;
}

#endif // XAODDITAU_VERSIONS_DITAUCONTAINER_V1_H