// Dear emacs, this is -*- c++ -*-
// $Id: xAODTauJetAuxContainerCnv.h 581660 2014-02-05 15:52:29Z janus $
#ifndef XAODDITAUATHENAPOOL_XAODDITAUJETAUXCONTAINERCNV_H
#define XAODDITAUATHENAPOOL_XAODDITAUJETAUXCONTAINERCNV_H

// Gaudi/Athena include(s):
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

// EDM include(s):
#include "xAODTau/DiTauJetAuxContainer.h"

/// Base class for the converter
typedef T_AthenaPoolCustomCnv< xAOD::DiTauJetAuxContainer,
                               xAOD::DiTauJetAuxContainer >
   xAODDiTauJetAuxContainerCnvBase;

/**
 *  @short POOL converter for the xAOD::DiTauJetAuxContainer class
 *
 *         This is the converter doing the actual schema evolution
 *         of the package... The converter for xAOD::DiTauJetContainer
 *         doesn't do much, as the "interface classes" don't contain
 *         too much/any payload. Most of the payload is in the auxiliary
 *         containers like this one.
 *
 * @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
 * @author Michel Janus <janus@cern.ch>
 *
 * $Revision: 581660 $
 * $Date: 2014-02-05 10:52:29 -0500 (Wed, 05 Feb 2014) $
 */
class xAODDiTauJetAuxContainerCnv :
   public xAODDiTauJetAuxContainerCnvBase {

   // Declare the factory as our friend:
   friend class CnvFactory< xAODDiTauJetAuxContainerCnv >;

protected:
   /// Converter constructor
   xAODDiTauJetAuxContainerCnv( ISvcLocator* svcLoc );

   /// Function preparing the container to be written out
   virtual xAOD::DiTauJetAuxContainer*
   createPersistent( xAOD::DiTauJetAuxContainer* trans );
   /// Function reading in the object from the input file
   virtual xAOD::DiTauJetAuxContainer* createTransient();

}; // class xAODDiTauJetAuxContainerCnv

#endif // XAODDITAUATHENAPOOL_XAODDITAUJETAUXCONTAINERCNV_H
