// Dear emacs, this is -*- c++ -*-
// $Id: xAODTauJetContainerCnv.h 632169 2014-11-29 15:13:34Z krasznaa $
#ifndef XAODDITAUEVENTATHENAPOOL_XAODDITAUJETCONTAINERCNV_H
#define XAODDITAUEVENTATHENAPOOL_XAODDITAUJETCONTAINERCNV_H

// Gaudi/Athena include(s):
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

// EDM include(s):
#include "xAODTau/DiTauJetContainer.h"

/// Type definition for the converter's base
typedef T_AthenaPoolCustomCnv< xAOD::DiTauJetContainer,
                               xAOD::DiTauJetContainer >
   xAODDiTauJetContainerCnvBase;

/**
 *  @short POOL converter for the xAOD::DiTauJetContainer class
 *
 *         Simple converter class making the xAOD::DiTauJetContainer
 *         class known to POOL.
 *
 * @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
 * @author Michel Janus <janus@cern.ch>
 *
 * $Revision: 632169 $
 * $Date: 2014-11-29 10:13:34 -0500 (Sat, 29 Nov 2014) $
 */
class xAODDiTauJetContainerCnv : public xAODDiTauJetContainerCnvBase {

   // Declare the factory as our friend:
   friend class CnvFactory< xAODDiTauJetContainerCnv >;

public:
   /// Converter constructor
   xAODDiTauJetContainerCnv( ISvcLocator* svcLoc );

   /// Re-implemented function in order to get access to the SG key
   virtual StatusCode createObj( IOpaqueAddress* pAddr, DataObject*& pObj );

   /// Function preparing the container to be written out
   virtual xAOD::DiTauJetContainer*
   createPersistent( xAOD::DiTauJetContainer* trans );
   /// Function reading in the persistent object
   virtual xAOD::DiTauJetContainer* createTransient();

private:
   /// StoreGate key of the container just being created
   std::string m_key;

}; // class xAODDiTauJetContainerCnv

#endif // XAODDITAUEVENTATHENAPOOL_XAODDITAUJETCONTAINERCNV_H